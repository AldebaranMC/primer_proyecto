package PrimerProyecto;

public class Cuadrado {
    private double lado;

    public Cuadrado() {
    }

    public Cuadrado(double lado) {
        this.lado = lado;
    }

    public double getLado() {
        return lado;
    }

    public void setLado(double lado) {
        this.lado = lado;
    }
    
    //@Override
    public double perimetro(){
        return this.getLado()*4;
    }
    
    //@Override
    public double area(){
        return this.getLado()*this.getLado();
    }

    @Override
    public String toString() {
        return "Cuadrado{" + "lado=" + lado + '}';
    }
    
    
    //@Override
    public void display(){
        System.out.println(this.toString()
                            +" Area: " +this.area()
                            +" Perimetro: " +this.perimetro());
    }
}